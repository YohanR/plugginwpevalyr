<?php
/*
Plugin Name: page admin pour la liste des entreprises
Description: un plugin qui permet de faire un CRUD sur la liste des entreprises que j'ai croisé
Author: Marvin Massot
 */

function admin_compagny_list()
{
    add_menu_page(
        __('Admin CRUD entreprises BDD', 'my-project'), // Page Title
        __('Admin CRUD entreprises BDD', 'my-project'), // Menu Title
        'manage_options', // Capability
        'admin compagny list', // Menu Slug
        'my_admin_page_contents', // Page Callback function
        'dashicons-image-crop', // Dashicon
        2 // Menu Position
    );
}
function add_entreprises()
{
    global $wpdb; // On se connecte à la base de données du site
    $annuaires = $wpdb->get_results("
    SELECT nom_entreprise,
    localisation_entreprise,
    prenom_contact,
    nom_contact,
    mail_contact
    FROM wp_annuaire;
");
}


function my_admin_page_contents()
{
?>
    <?php
    global $wpdb; // On se connecte à la base de données du site
    $annuaires = $wpdb->get_results("
    SELECT
    id,
    nom_entreprise,
    localisation_entreprise,
    prenom_contact,
    nom_contact,
    mail_contact
    FROM wp_annuaire;
");
    // print_r($annuaire);
    ?>
    <h1 class='text-center h1'>Ici vous pouvez modifier, créer et supprimer les entreprises sauvergardées en BDD.</h1>
    <section class='container'>
        <form method="POST" action=''>
            <div class="form-group">
                <label for="exampleInputName">Nom de l'entreprise</label>
                <input type="text" name='NomEntreprise' class="form-control" id="exampleInputName" aria-describedby="namelHelp" placeholder="Entrer le nom de l'entreprise">
            </div>
            <div class="form-group">
                <label for="exampleInputLocalisation">Localisation</label>
                <input type="text" name='LocalisationEntreprise' class="form-control" id="exampleInputLocalisation" aria-describedby="LocalisationHelp" placeholder="Ville">
            </div>
            <div class="form-group">
                <label for="exampleInputFirstNameContact">Prénom contact</label>
                <input type="text" name='PrenomContact' class="form-control" id="exampleInputFirstNameContact" aria-describedby="firstNameContactHelp" placeholder="Entrer le prénom du contact">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Nom contact</label>
                <input name='NomContact' class="form-control" id="exampleInputEmail1" aria-describedby="namelHelp" placeholder="Enter Name">
                <small id="" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input name='EmailContact' type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <button type="submit" name='ok' value='ok' class="btn btn-primary">Ajouter</button>
        </form>
    </section>
    <section class="container">
        <div class="row">
            <div class="col-12">
                <table class="table table-hover table-dark" data-toggle="table" data-search="true" data-pagination="true">
                    <thead>
                        <tr>
                            <th data-sortable="true" data-field="Nom entreprise">Nom entreprise</th>
                            <th data-sortable="true" data-field="Localisation">Localisation</th>
                            <th data-sortable="true" data-field="Prénom">Prénom contact</th>
                            <th data-sortable="true" data-field="Nom">Nom contact</th>
                            <th data-sortable="true" data-field="Mail">Mail contact</th>
                            <th data-sortable="true" data-field="Edition">Edition</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        // On boucle sur tous les articles
                        foreach ($annuaires as $annuaire) {
                        ?>
                            <tr>
                                <td><?= $annuaire->nom_entreprise ?></td>
                                <td><?= $annuaire->localisation_entreprise ?></td>
                                <td><?= $annuaire->prenom_contact ?></td>
                                <td><?= $annuaire->nom_contact ?></td>
                                <td><?= $annuaire->mail_contact ?></td>
                                <td>
                                    <form method="post" action=''>
                                        <button type="button" data-toggle="modal" data-target="#<?php echo $annuaire->nom_entreprise;?>" value=""  class="btn btn-success mr-3">Modifier</button>
                                        <button type="submit" class="btn  btn-danger" name="supprimer" value="<?php echo $annuaire->id; ?>">Supprimer</button>
                                    </form>
                                    <!-- Modal -->
                                    <div class="modal fade" id="<?php echo $annuaire->nom_entreprise;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <section class='container'>
                                                    <form method="POST" action=''>
                                                        <div class="form-group">
                                                            <label for="exampleInputName">Nom de l'entreprise</label>
                                                            <input type="text" value='<?php echo $annuaire->nom_entreprise;?>' name='NomEntreprise' class="form-control" id="exampleInputName" aria-describedby="namelHelp" placeholder="<?php echo $annuaire->nom_entreprise;?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputLocalisation">Localisation</label>
                                                            <input type="text" value='<?php echo $annuaire->localisation_entreprise;?>' name='LocalisationEntreprise' class="form-control" id="exampleInputLocalisation" aria-describedby="LocalisationHelp" placeholder="<?php echo $annuaire->localisation_entreprise;?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputFirstNameContact">Prénom contact</label>
                                                            <input type="text" value='<?php echo $annuaire->prenom_contact;?>'  name='PrenomContact' class="form-control" id="exampleInputFirstNameContact" aria-describedby="firstNameContactHelp" placeholder="<?php echo $annuaire->prenom_contact;?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Nom contact</label>
                                                            <input name='NomContact' value='<?php echo $annuaire->nom_contact;?>' class="form-control" id="exampleInputEmail1" aria-describedby="namelHelp" placeholder="<?php echo $annuaire->nom_contact;?>">
                                                            <small id="" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Email address</label>
                                                            <input name='EmailContact'value='<?php echo $annuaire->mail_contact;?>'  type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="<?php echo $annuaire->mail_contact;?>">
                                                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                                        </div>
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type="submit" name='modifier' value='<?php echo $annuaire->id;?>' class="btn btn-primary">Modifier</button>
                                                    </form>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2               +poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <!-- Insérer cette balise "script" après celle de Bootstrap -->
    <script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.16.0/dist/locale/bootstrap-table-fr-FR.min.js"></script>   

<?php
}

@$nom_entreprise = $_POST['NomEntreprise'];
@$localisation_entreprise = $_POST['LocalisationEntreprise'];
@$prenom_contact = $_POST['PrenomContact'];
@$nom_contact = $_POST['NomContact'];
@$email_contact = $_POST['EmailContact'];
@$ok = $_POST['ok'];
@$deleteid = $_POST['supprimer'];
@$modifier = $_POST['modifier'];
?>


<?php
if ($ok == 'ok') {
    global $wpdb; // On se connecte à la base de données du site
    $wpdb->insert(
        'wp_annuaire',
        array(
            'nom_entreprise' => $nom_entreprise,
            'localisation_entreprise' => $localisation_entreprise,
            'prenom_contact' => $prenom_contact,
            'nom_contact' => $nom_contact,
            'mail_contact' => $email_contact
        )
    );
}

if (isset($deleteid)) {
    global $wpdb;
    $wpdb->delete('wp_annuaire', array('id' => $deleteid));
}


if (isset($modifier)) {
    $wpdb->update(
        'wp_annuaire',
        array(
            'nom_entreprise' => $nom_entreprise,
            'localisation_entreprise' => $localisation_entreprise,
            'prenom_contact' => $prenom_contact,
            'nom_contact' => $nom_contact,
            'mail_contact' => $email_contact
        ),
        array('id'=>$modifier))
    ;
}

add_action('admin_menu', 'admin_compagny_list');

<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=./1.0">
    <title>Pokedex Entreprise </title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- Insérer cette balise "link" après celle de Bootstrap -->
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
    <link href="./style.css" rel="stylesheet">

</head>

<?php
/*
Plugin Name: liste des entreprises de la bdd
Description: un plugin qui affiche la liste des entreprises de ma base de données
Author: Rucinski Yohan
*/
function shortcode_entreprises(){
global $wpdb; // On se connecte à la base de données du site
$annuaires = $wpdb->get_results("
SELECT nom_entreprise,
localisation_entreprise,
prenom_contact,
nom_contact,
mail_contact
FROM wp_annuaire;
");
// print_r($annuaire);
?>

<body>
<section class="container">
        <div class="row">
            <div class="col-12">
                <table class="table table-hover table-dark" data-toggle="table" data-search="true" data-pagination="true">
                    <thead>
                        <tr>
                            <th data-sortable="true" data-field="Nom entreprise">Nom entreprise</th>
                            <th data-sortable="true" data-field="Localisation">Localisation</th>
                            <th data-sortable="true" data-field="Prénom">Prénom contact</th>
                            <th data-sortable="true" data-field="Nom">Nom contact</th>
                            <th data-sortable="true" data-field="Mail">Mail contact</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        // On boucle sur tous les articles
                        foreach($annuaires as $annuaire){
                        ?>
                            <tr>
                                <td><?= $annuaire->nom_entreprise ?></td>
                                <td><?= $annuaire->localisation_entreprise ?></td>
                                <td><?= $annuaire->prenom_contact ?></td>
                                <td><?= $annuaire->nom_contact ?></td>
                                <td><?= $annuaire->mail_contact ?></td>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <!-- Insérer cette balise "script" après celle de Bootstrap -->
    <script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.16.0/dist/locale/bootstrap-table-fr-FR.min.js"></script>   
</body>
</html>`

    <?php
}

add_shortcode('bienvenue', 'shortcode_entreprises');
?>
   